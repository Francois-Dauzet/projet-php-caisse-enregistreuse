<?php

declare (strict_types=1);

// Variables
(bool) $finSaisie = true;
(int) $recapCafe = 0;
(int) $recapSoda = 0;
(int) $recapBiere = 0;
(float) $recapHT = 0.0;
(float) $recapTTC = 0.0;
(float) $recapTVA = 0.0;

// (array) $infoClients = [];

// créer un tableau avec nom de produit, sa taxe et sont taux de tva
(array) $produits = [
    'cafe' => ['HT' => 1.00, 'TTC' => 1.10, 'TVA' => 10],
    'soda' => ['HT' => 2.00, 'TTC' => 2.20, 'TVA' => 10],
    'biere' => ['HT' => 2.00, 'TTC' => 2.40, 'TVA' => 20]
];



// Boucle de demande de saisie avec récapitulatif de la caisse
while ($finSaisie == true){

    // (array) $infoClient = [
    //     'cafe' => 0,
    //     'soda' => 0,
    //     'biere' => 0,
    //     'HT' => 0.0,
    //     'TTC' => 0.0
    // ];

    (int) $saisieCafe = -1;
    (int) $saisieSoda = -1;
    (int) $saisieBiere = -1;
    (int) $saisieArgent = 0;
    (int) $saisieArgentHT = 0;
    (int) $saisieArgentTTC = 0;
    (float) $renduArgent = 0.0;
    (float) $totalCafeHT = 0.0;
    (float) $totalSodaHT = 0.0;
    (float) $totalBiereHT = 0.0;
    (float) $totalHT = 0.0;
    (float) $totalCafeTTC = 0.0;
    (float) $totalSodaTTC = 0.0;
    (float) $totalBiereTCC = 0.0;
    (float) $totalTTC = 0.0;
    (string) $choixFin = "";

    // demander le nombre de consomation et calculer un total
    while (!is_numeric($saisieCafe) || $saisieCafe < 0){
        print(PHP_EOL . 'Combien de café avez vous servi ? ');
        $saisieCafe = trim(fgets(STDIN));
    }

    $recapCafe = $recapCafe + $saisieCafe;

    while (!is_numeric($saisieSoda) || $saisieSoda < 0){
        print('Combien de soda avez vous servi ? ');
        $saisieSoda = trim(fgets(STDIN));
    }

    $recapSoda = $recapSoda + $saisieSoda;

    while (!is_numeric($saisieBiere) || $saisieBiere < 0){
        print('Combien de biere avez vous servi ? ');
        $saisieBiere = trim(fgets(STDIN));
    }

    $recapBiere = $recapBiere + $saisieBiere;

    print(PHP_EOL);

    $totalCafeHT = $produits['cafe']['HT'] * $saisieCafe;
    $totalSodaHT = $produits['soda']['HT'] * $saisieSoda;
    $totalBiereHT = $produits['biere']['HT'] * $saisieBiere;
    $totalHT = $totalCafeHT + $totalSodaHT + $totalBiereHT;

    $recapHT = $recapHT + $totalHT;

    $totalCafeTTC = $produits['cafe']['TTC'] * $saisieCafe;
    $totalSodaTTC = $produits['soda']['TTC'] * $saisieSoda;
    $totalBiereTTC = $produits['biere']['TTC'] * $saisieBiere;
    $totalTTC = $totalCafeTTC + $totalSodaTTC + $totalBiereTTC;

    $recapTTC = $recapTTC + $totalTTC;

    $recapTVA = $recapTTC - $recapHT;

    // $infoClients[] = $infoClient;

    // print_r($produits);
    print('Prix : ' . $totalTTC . ' € TTC' . PHP_EOL);

    // mettre en place le rendu de monnaie
    while ($saisieArgent < $totalTTC){
        (float) $differenceRendu = 0;
        print(PHP_EOL . 'Argent donner par le client ? ');
        $saisieArgent = intval(trim(fgets(STDIN)));
    if ($saisieArgent < $totalTTC){
        $differenceRendu = $totalTTC - $saisieArgent;
        print('Attention il manque : ' . $differenceRendu);
    }
    }

    $renduArgentHT = $saisieArgent - $totalHT;
    $renduArgentTTC = $saisieArgent - $totalTTC;

    print('Argent à rendre : ' . $renduArgentTTC . ' € TTC' . PHP_EOL);

    // éditer un ticket de caisse avec montant hors taxe et tva
    print(PHP_EOL . '////////Ticket de caisse////////' . PHP_EOL);
    printf(PHP_EOL . '--------Produits--------' . PHP_EOL . '        Café X %d' . PHP_EOL . '        Soda X %d' . PHP_EOL . '        Bière X %d' . PHP_EOL . PHP_EOL . '--------Récapitulatif--------' . PHP_EOL .
        '        Réglement : %.2g €' . PHP_EOL . '        Rendu caisse : %g €' . PHP_EOL . PHP_EOL . '--------Total--------' . PHP_EOL .
        '        Montant HT : %.2g €' . PHP_EOL . '        Montant TTC : %g €' . PHP_EOL,
        $saisieCafe,
        $saisieSoda,
        $saisieBiere,
        $saisieArgent,
        $renduArgentTTC,
        $totalHT,
        $totalTTC
    );

    while ($choixFin != 'oui' && $choixFin != 'non'){
        print(PHP_EOL . 'Avez vous d\'autres clients à saisir ("oui" ou "non") ? ');
        $choixFin = strval(trim(fgets(STDIN)));
    }

    $finSaisie = ($choixFin == 'oui');
}

// éditer un récapitulatif avec montant hors taxe et tva
print(PHP_EOL . '////////Récapitulatif////////' . PHP_EOL);
printf(PHP_EOL . '--------Produits--------' . PHP_EOL . '        Café X %d' . PHP_EOL . '        Soda X %d' . PHP_EOL . '        Bière X %d' . PHP_EOL . PHP_EOL .
    '--------Total--------' . PHP_EOL .
    '        Total HT : %.2g €' . PHP_EOL . '        Total TTC : %g €' . PHP_EOL . '        Total TVA : %g €',
    $recapCafe,
    $recapSoda,
    $recapBiere,
    $recapHT,
    $recapTTC,
    $recapTVA
);


// calcul de tableau avec nombre de produit vendu et l'etat de caisse récapitulatif

// Café = 1.00 HT / 1.10 TTC 10% TVA
// Soda = 2.00 HT / 2.20 TTC 10% TVA
// Bière = 2.00 HT / 2.40 TTC 20% TVA